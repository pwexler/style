********************
 Python Style Guide
********************

Use `Pep8`_
===========

The importance of programming style is often underestimated.
An exceedingly conservative estimate is that 
for each line of code that you write,
you will read that line 128 times.
Therefore, barring other constraints, optimize your code for readability.
Strive to make your code exceedingly readable.

Towards this end, 
I recommend following the guidelines set out in `PEP8`_.
Break the rules whenever you want to - but you need a good reason to do so.


- Limit lines to 79 chars (and long comment lines to 72 chars).

- Use 4 spaces per indentation level.

- Do not use tabs.

  Your editor probably can be configured 
  to manage the indent level using spaces.

- Manage the use of whitespace.
  Follow `Pep8`_.

Here is a simple guideline.  

- If a line of code is too long, 
  split it up into continuation lines.

Continuation Lines
==================

`Pep8`_ is not entirely explicit on the indentation of continuation lines.
Indeed, a variety of styles are shown.  
I recommend the following simple rule.

- Double indent every continuation line. 

This makes it very clear that a line is 
in fact a continuation line and not a new line of code.
Further, the resulting shape of the code reflects its
structure both functionally and logically
and is independent of the names being used in the code.

My goal here is that I want to read code the way I read music
because I believe there is a close analogy between 
reading a program and reading music.
When I read music wrong, 
I'll play the wrong note or the wrong duration; 
when I read a program wrong, 
I'll misinterpret its intent.

I played classical clarinet professionally for many years 
and I observed an inverse relationship between 
the quality of the music manuscript 
and the number of mistakes made during rehearsals and performances.
By analogy, when a program has poor style, 
we will spend more time understanding, debugging, or maintaining it.

A "Breitkopf und Hartels" edition was much easier to play from 
than an "International Music Company" edition.  
I believe it mostly depends on the use of whitespace!

Music notation has both formal rules and informal rules.
For example, formally, a note's duration is indicated 
by the graphic used for its notehead.
Informally, longer notes are followed by more whitespace.
International Music Company editions save paper 
and cheat on whitespace and are harder to read.

In other words, 
when there exists a redundancy between the formal and informal rules,
the notation becomes easier to read.
Easier to see at a glance.

While western musical notation has had hundreds of years to perfect itself, 
programming is still relatively new.
`PEP8`_ itself has undergone recent changes and it continues to evolve.

In programming, the formal rules are the language syntax;
the informal rules are the style and use of whitespace.
 
Usually I need to scan code looking for something or other.
I don't want to have to read all the code.
I don't want to wade through it,
I want to fly over it.
That is why I am persnickety about using whitespace 
and distinguishing continuation lines from normal code.

If the function parameters all fit on one line - great!
If not, each one goes double indented on its own line.
This way the vertical layout is used to advantage.  
When things are on one line, 
the length of the line depends on the names being referenced
and offers limited extra information.
Whereas the vertical length of a parameter block is 
directly related to the number of parameters.

Example
=======

To see why 79 characters per line is better, 
make the screen you are viewing this page on narrow
enough to cause a horizontal scrollbar to be displayed.

::

  >>> my_foo = FOO
  >>> if my_foo:
  >>>     result = some_function(
  >>>             how_nice,
  >>>             that_python,
  >>>             allows_a,
  >>>             trailling_comma,
  >>>             )
  >>> else:
  >>>     another_function(
  >>>             some_arg=1,
  >>>             another_arg=(
  >>>                     "This long string"
  >>>                     " is wrapped in parentheses,"
  >>>                     " double indented another level,"
  >>>                     " and we can see it all at once."
  >>>                     ),
  >>>             last_arg={},
  >>>             )
      
Here is the same code 
(without the parentheses which were required 
to break up the value for *another_arg*
onto separate lines)

::
 
  >>> my_foo = FOO
  >>> if my_foo:
  >>>     result = some_function(how_nice, that_python, allows_a, trailling_comma, )
  >>> else:
  >>>     another_function(some_arg=1, 
  >>>                      another_arg="This long string is wrapped in parentheses, double indented another level, and we can see it all at once.",
  >>>                      last_arg={},) 

.. _PEP8: https://www.python.org/dev/peps/pep-0008/
